var JSON_MACHINE;
var JSON_SENSOR;

$(document).ready(function() 
{
    $('#button1').click(function(){
        test("Nova sensor");
    }
    );
    $('#button2').click(function(){
        test("Nova Maquina");
    }
    );
    
});


test = function(value)
{
    console.log("destroy");
    $("#form1").alpaca("destroy");
    console.log("create "+ value);

    var autorization = true;
    var list_sensores = 
    {
        "Vanilla": "Vanilla Flavor",
        "Chocolate": "Chocolate Flavor",
        "Strawberry": "Strawberry Flavor",
        "Mint": "Mint Flavor"
    };

    JSON_MACHINE = 
    {
        "schema": 
        {
            "title": "MACHINES",
            "type": "object",
            "properties": 
            {
                "_id": 
                {
                    "type": "string",
                    "title": "#ID"
                },
                "title": 
                {
                    "type": "string",
                    "title": "Name"
                },
                "description": 
                {
                    "type": "string",
                    "title": "Description"
                },
                "sensors": 
                {
                    "type": "array",
                    "title": "Sensors",
                    "items": 
                    {
                        "title": "Sensor nº",
                        "type": "object",
                        "properties": 
                        {
                            "id_sensor":
                            {
                                "type": "string",
                                "title": "#ID Sensor"
                            },
                            "unit":
                            {
                                "type": "string",
                                "title": "Unit"
                            }
                        }
                    }
                }
            }
        },
        "options": 
        {
            "form":
            {
                "buttons":
                {
                    "submit":
                    {
                        "title": "Send Form Data",
                        "click": function() {var val = this.getValue(); console.log(val);}
                    }
                }
            },
            "fields": 
            {
                "_id":
                {
                    "disabled":true
                },
                "sensors":
                {
                    "items":
                    {
                        "fields":
                        {
                            "id_sensor":
                            { 
                                "type": "select",
                                "dataSource":list_sensores
                            }
                        },
                        "disabled": !autorization
                    },
                    "toolbarSticky": autorization
                    
                }
            },
            "disabled": !autorization
        }
    };
    JSON_SENSOR = 
    {
        "schema": 
        {
            "title": "SENSOR",
            "type": "object",
            "properties": 
            {
                "_id": 
                {
                    "type": "string",
                    "title": "#ID"
                },
                "title": 
                {
                    "type": "string",
                    "title": "Name"
                },
                "description": 
                {
                    "type": "string",
                    "title": "Description"
                },
                "gateway": 
                {
                    "type": "integer",
                    "title": "Gateway"
                },
                "port": 
                {
                    "type": "integer",
                    "title": "Port"
                },
                "active": 
                {
                    "type": "boolean",
                    "title": "Active"
                }   
            }
        },
        "options": 
        {
            "form":
            {
                "buttons":
                {
                    "submit":
                    {
                        "title": "Send Form Data",
                        "click": function() {var val = this.getValue(); console.log(val);}
                    }
                }
            },
            "fields": 
            {
                "_id":
                {
                    "disabled":true
                }
            },
            "disabled": !autorization
        }
    };


if(value=="Nova sensor")
{
    $("#form1").alpaca(
        JSON_SENSOR);
}
else
{
    $("#form1").alpaca(
        JSON_MACHINE);
}


}